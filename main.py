import speech_recognition as sr
import time
import os

import vocal_commands

DEBUG_ON = True
stop_listening_val = False
debug = lambda x : print("DEBUG : " + x) if DEBUG_ON else None

def ListMicrophoneIndex():
  for index, name in enumerate(sr.Microphone.list_microphone_names()):
    print("Microphone with name \"{1}\" found for `Microphone(device_index={0})`".format(index, name))

# this is called from the background thread
def callback(recognizer, audio):
  global stop_listening_val
  try:
    text = recognizer.recognize_google(audio)
    debug(text)
    
    if text == "stop listening":
      stop_listening_val= True
      print("Stopping engine")
      stop_listening(wait_for_stop=False)
      return

    commands = vocal_commands.InterpretCommands(text)
    if commands != None:
      for command in commands:
        os.system(command)
    
  except sr.UnknownValueError:
    print("Audio not recognized")
  except sr.RequestError as e:
    print("Could not request results from Google Speech Recognition service; {0}".format(e))


r = sr.Recognizer()
m = sr.Microphone(device_index=6)
with m as source:
  r.adjust_for_ambient_noise(source)  # we only need to calibrate once, before we start listening

# start listening in the background (note that we don't have to do this inside a `with` statement)
stop_listening = r.listen_in_background(m, callback)
print("Listening...")

while True:
  if stop_listening_val:
    break
  time.sleep(1)