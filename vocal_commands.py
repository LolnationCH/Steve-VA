import json
import operator
from typing import List

commands = []

with open("actions.json") as f:
  commands = json.load(f)

def InterpretCommands(text: str) -> List[str]: 
  wordSplit = text.lower().split(" ")
  res_value = 0
  res_action = None
  for c in commands:
    res = set(c["vocal"]).intersection(wordSplit)
    temp_res_value = len(res) / len(c["vocal"])
    if res_value < temp_res_value and temp_res_value > 0.5:
      res_value = temp_res_value
      res_action = c["action"]
  
  return res_action